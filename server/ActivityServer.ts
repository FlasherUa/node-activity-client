/// <reference path="../ActivityStepFactory_types.d.ts" />
//@ts-ignore

import Activity from "../Activity";
import {ActivityClient} from "../client/ActivityClient";
export  class ActivityServer extends  ActivityClient implements I_ActivityStepFactory {
    protected isServer=true

    /**
     * Return activity to client
     * @param activity_name
     * @param stepCfg
     * @param state
     */
    async runStepRemote(activity_name: string, stepCfg: TActivityStep,
                       state: T_ActivityStepState): Promise<T_ActivityStepState>  {
        //return state to client
        const data: I_ActivityStepMessage = {
            activity: activity_name,
            state,
            step: stepCfg.no,
            resultType: "changeActor"
        }

        return data

    }

}
