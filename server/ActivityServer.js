/// <reference path="../ActivityStepFactory_types.d.ts" />
//@ts-ignore
import { ActivityClient } from "../client/ActivityClient";
export class ActivityServer extends ActivityClient {
    constructor() {
        super(...arguments);
        this.isServer = true;
    }
    /**
     * Return activity to client
     * @param activity_name
     * @param stepCfg
     * @param state
     */
    async runStepRemote(activity_name, stepCfg, state) {
        //return state to client
        const data = {
            activity: activity_name,
            state,
            step: stepCfg.no,
            resultType: "changeActor"
        };
        return data;
    }
}
<<<<<<< HEAD
//# sourceMappingURL=ActivityServer.js.map
=======
>>>>>>> e84d40ffa828f07a4c17a2a7f652d87612560b9b
