import Resolver_shopify_checkout_validate from "../../serverActions/Resolver_shopify_checkout_validate";
import Resolver_shopify_checkout_pay from "../../serverActions/Resolver_shopify_checkout_pay";
import Resolver_shopify_checkout_addOrder2Shopify from "../../serverActions/Resolver_shopify_checkout_addOrder2Shopify";

import {I_ActivityStepMessage, I_ActivityStepResult} from "../ActivityStepFactory_types";

const actions: { [key: string]: Function } = {
    Resolver_shopify_checkout_validate,
    Resolver_shopify_checkout_pay,
    Resolver_shopify_checkout_addOrder2Shopify
}

export default function (route: string, route_data: string, state: any): Promise<I_ActivityStepResult> | I_ActivityStepResult {
     const action = "Resolver_" + route.replace("/","_")
    if (actions[action]) {
        //prepare action interface
        return actions[action](null, state, {})
    }
    return {
        resultType: "error", state: {error: "Route not found " + route}
    }
}
