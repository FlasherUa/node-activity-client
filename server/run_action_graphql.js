import Resolver_shopify_checkout_validate from "../../serverActions/Resolver_shopify_checkout_validate";
import Resolver_shopify_checkout_pay from "../../serverActions/Resolver_shopify_checkout_pay";
import Resolver_shopify_checkout_addOrder2Shopify from "../../serverActions/Resolver_shopify_checkout_addOrder2Shopify";
const actions = {
    Resolver_shopify_checkout_validate,
    Resolver_shopify_checkout_pay,
    Resolver_shopify_checkout_addOrder2Shopify
};
export default function (route, route_data, state) {
    const action = "Resolver_" + route.replace("/", "_");
    if (actions[action]) {
        //prepare action interface
        return actions[action](null, state, {});
    }
    return {
        resultType: "error", state: { error: "Route not found " + route }
    };
}
//# sourceMappingURL=run_action_graphql.js.map