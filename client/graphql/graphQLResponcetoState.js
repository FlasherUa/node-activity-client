/**
 * Adaptor
 * @param result
 */
export default function (result) {
    return {
        activity: result.data.activity,
        state: result.data.state,
        step: result.data.step,
    };
}
//# sourceMappingURL=graphQLResponcetoState.js.map