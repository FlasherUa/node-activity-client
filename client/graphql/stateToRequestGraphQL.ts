import {I_ActivityStepMessage} from "../ActivityStepFactory_types";

export default function (data: I_ActivityStepMessage): string {
    return `{ca { activity { step(
        state:"${JSON.stringify(data.state)}"
        activity:"${data.activity}"
        step:"${data.step}"
        {
            step
            state
            activity
        }}}}`
}