export default function (data) {
    return `{ca { activity { step(
        state:"${JSON.stringify(data.state)}"
        activity:"${data.activity}"
        step:"${data.step}"
        {
            step
            state
            activity
        }}}}`;
}
//# sourceMappingURL=stateToRequestGraphQL.js.map