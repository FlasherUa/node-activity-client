import {I_Fetcher_Result} from "../Server/fetcherGQL";
import {I_ActivityStepMessage} from "./ActivityStepFactory_types";

/**
 * Adaptor
 * @param result
 */
export default function (result: I_Fetcher_Result): I_ActivityStepMessage {

    return {
        activity: result.data.activity,
        state: result.data.state,
        step: result.data.step,
    }
}