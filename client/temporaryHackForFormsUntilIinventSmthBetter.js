export default (state, completeAction, errorAction) => {
    if (typeof state.layout !== "undefined") {
        if (typeof state.layout.popup !== "undefined") {
            if (typeof state.layout.popup.componentState === "undefined") {
                state.layout.popup.componentState = { onSubmit: completeAction };
            }
            else {
                state.layout.popup.componentState.onSubmit = completeAction;
            }
        }
    }
    return state;
};
//# sourceMappingURL=temporaryHackForFormsUntilIinventSmthBetter.js.map