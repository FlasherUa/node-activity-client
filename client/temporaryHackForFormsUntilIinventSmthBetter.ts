import {I_AppStore} from "../../../AppStore";

export default (state: Partial<I_AppStore>, completeAction: Function, errorAction:Function): Partial<I_AppStore> => {
    if (typeof state.layout !== "undefined") {
        if (typeof state.layout.popup !== "undefined") {
            if (typeof state.layout.popup.componentState === "undefined") {
                state.layout.popup.componentState = {onSubmit: completeAction}
            } else {
                state.layout.popup.componentState.onSubmit = completeAction
            }
        }
    }
    return state
}