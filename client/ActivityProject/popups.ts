import {showPopup} from "../../../Components/Core/Layout/Popup_1";
import CardDataForm from "../../../Components/Pages/Basket/Order/CardDataForm";


export default function (state: { component:string }) {


    showPopup({
        show: true,
        component:getComponentByName(state.component)
    })


}

const components={
    CardDataForm
}

function getComponentByName (name:string):any {
    //@ts-ignore
    return components[name]
}