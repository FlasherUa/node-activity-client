///<reference path="../ActivityStepFactory_types.d.ts" />

//@ts-ignore
import cfg from '../cfg/client';
import runCode from '../utilites/runCode';

import Activity from '../Activity';
import decisionCase from '../utilites/decisionCase';

export class ActivityClient extends Activity implements I_ActivityStepFactory {
    stateToRequest: Function;
    //conenctor to activity server part
    fetcher: F_AC_fetcher;
    protected isServer = false;

    constructor(
        cfg,
        /*stateToRequest,*/ getAppState: F_GetAppState,
        setAppState: F_SetAppState,
        fetcher: F_AC_fetcher,
        runRoute: F_RunRoute,
        Utils?: {activity: Function},
    ) {
        super(cfg);
        this.fetcher = fetcher;
        this.getAppState = getAppState;
        this.setAppState = setAppState;
        this.runRoute = runRoute;
        // this.Utils =Utils
    }

    /**
     * call server
     * @param activity_name
     * @param stepCfg
     * @param state
     */

    async runStepRemote(
        activity_name: string,
        stepCfg: TActivityStep,
        state: T_ActivityStepState,
    ): Promise<T_ActivityStepState> {
        //call server
        const activitySt: I_ActivityStepMessage = {
            activity: activity_name,
            step: stepCfg.no,
            state: state,
            resultType: null,
        };
        const res = await this.fetcher(activitySt);
<<<<<<< HEAD
=======
        debugger
>>>>>>> e84d40ffa828f07a4c17a2a7f652d87612560b9b
        //double iteration bug fix - save remote steps
        res.step = Number(res.step) - 1;
        //it`s good
        if (res.resultType === 'changeActor') {
            res.resultType = 'ok';
        }
        //@ts-ignore
        return res;
    }
}
