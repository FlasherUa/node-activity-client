import App from "../../../App";
import {I_AppStore} from "../../../AppStore";
import temporaryHackForFormsUntilIinventSmthBetter from "./temporaryHackForFormsUntilIinventSmthBetter";

export default function (stateApp0:Partial<I_AppStore>, completeAction:Function,errorAction:Function){
    App.setStateStatic((prevSate: I_AppStore) => {
        const  stateApp = temporaryHackForFormsUntilIinventSmthBetter(stateApp0, completeAction, errorAction)
        return stateApp
    })
}