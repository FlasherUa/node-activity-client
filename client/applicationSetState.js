import App from "../../../App";
import temporaryHackForFormsUntilIinventSmthBetter from "./temporaryHackForFormsUntilIinventSmthBetter";
export default function (stateApp0, completeAction, errorAction) {
    App.setStateStatic((prevSate) => {
        const stateApp = temporaryHackForFormsUntilIinventSmthBetter(stateApp0, completeAction, errorAction);
        return stateApp;
    });
}
//# sourceMappingURL=applicationSetState.js.map