///<reference path="../ActivityStepFactory_types.d.ts" />
import Activity from '../Activity';
export class ActivityClient extends Activity {
    constructor(cfg,
    /*stateToRequest,*/ getAppState, setAppState, fetcher, runRoute, Utils) {
        super(cfg);
        this.isServer = false;
        this.fetcher = fetcher;
        this.getAppState = getAppState;
        this.setAppState = setAppState;
        this.runRoute = runRoute;
        // this.Utils =Utils
    }
    /**
     * call server
     * @param activity_name
     * @param stepCfg
     * @param state
     */
    async runStepRemote(activity_name, stepCfg, state) {
        //call server
        const activitySt = {
            activity: activity_name,
            step: stepCfg.no,
            state: state,
            resultType: null,
        };
        const res = await this.fetcher(activitySt);
        //double iteration bug fix - save remote steps
        res.step = Number(res.step) - 1;
        //it`s good
        if (res.resultType === 'changeActor') {
            res.resultType = 'ok';
        }
        //@ts-ignore
        return res;
    }
}

