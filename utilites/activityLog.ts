export type logTypes = 'route' | 'step_result' | 'step_start' | 'decision' | 'error';

export default function(obj: any, type?: logTypes) {
    console.log(type, obj);
}
