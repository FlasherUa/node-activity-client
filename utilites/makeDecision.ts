import runCode from './runCode';
import activityLog from './activityLog';

export default async function(
    state: T_ActivityStepState,
    getAppState: F_GetAppState,
    setAppState: F_SetAppState,
    expression: string,
    cases: I_Activity_Cases,
): Promise<I_ActivityStepResult> {
    //run decision Code
    const decisionResult = await runDecisionCode(state, getAppState, setAppState, expression);
    //search decision cases
    const case_: I_Activity_Case = cases[decisionResult];

    //case not found, go back to activity flow
    activityLog({value: decisionResult, case: case_}, 'decision');
    if (!case_) {
        return {resultType: 'ok', state};
    }

    //case found - run a thread
    return {resultType: 'run', state: Object.assign(state, {activity: case_.id})};
}

async function runDecisionCode(state, getAppState, setAppState, expression): Promise<string> {
    const code = `
                   const __decision_value = ${expression}
                  return __decision_value`; //Object.assign({}, actionState, {__decision_value})

    const newActionState = await runCode(state, getAppState, setAppState, code);
    return '' + newActionState.state;
}
