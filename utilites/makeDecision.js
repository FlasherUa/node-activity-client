import runCode from './runCode';
import activityLog from './activityLog';
export default async function (state, getAppState, setAppState, expression, cases) {
    //run decision Code
    const decisionResult = await runDecisionCode(state, getAppState, setAppState, expression);
    //search decision cases
    const case_ = cases[decisionResult];
    //case not found, go back to activity flow
    activityLog({ value: decisionResult, case: case_ }, 'decision');
    if (!case_) {
        return { resultType: 'ok', state };
    }
    //case found - run a thread
    return { resultType: 'run', state: Object.assign(state, { activity: case_.id }) };
}
async function runDecisionCode(state, getAppState, setAppState, expression) {
    const code = `
                   const __decision_value = ${expression}
                  return __decision_value`; //Object.assign({}, actionState, {__decision_value})
    const newActionState = await runCode(state, getAppState, setAppState, code);
    return '' + newActionState.state;
}
