///<reference path="../ActivityStepFactory_types.d.ts" />
/**
 * Need to setup by client constructor
 *
 */
export let Utils = {};
/**
 *
 * @param actionState
 * @param getAppState
 * @param setState - application setState function
 * @param code - function code
 *
 * @param isPromise
 * @return T_ActivityStepState
 */
export default async function (actionState, getAppState, setState, code = "", isPromise = false) {
    /**
     * @return is next action state
     */
    if (isPromise) {
        code = `return new Promise((resolveAction,rejectAction)=>{
            ${code}
            })`;
    }
    const foo = new Function("actionState", "getAppState", "setAppState", "Utils", code);
    let state, resultType = "ok";
    try {
        state = foo(actionState, getAppState, setState, Utils);
        //if return is promise  - unwrap promise
        state = await Promise.resolve(state);
        //if state is I_ActivityStepResult
        if (state && state.resultType) {
            resultType = state.resultType;
            state = state.state;
        }
    }
    catch (e) {
        state = e;
        resultType = "error";
    }
    return { state: state, resultType: resultType };
}
