export default function (value, caseId) {
    return `const c = actionState. __decision_value === ${value};
    if (c) return {resultType:"run", state: Object.assign(actionState, {activity:'${caseId}'}) }
    else return   actionState`;
}
