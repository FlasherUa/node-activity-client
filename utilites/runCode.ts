///<reference path="../ActivityStepFactory_types.d.ts" />

//import {I_AppStore} from "../../../AppStore";
//import {I_ActivityStepResult, T_ActivityStepResultState, T_ActivityStepState} from "../ActivityStepFactory_types";
import {ActivityClient} from "../client/ActivityClient";
import Activity from "../Activity";

/**
 * Need to setup by client constructor
 *
 */
export let Utils: {
    /*activity runner  */
    activity?: (name: string, step: number , state: any , options: any )=>Promise<I_ActivityStepResult>
        } = {}

/**
 *
 * @param actionState
 * @param getAppState
 * @param setState - application setState function
 * @param code - function code
 *
 * @param isPromise
 * @return T_ActivityStepState
 */
export default async function (actionState: T_ActivityStepState, getAppState: F_GetAppState,
                               setState: F_SetAppState, code: string = "", isPromise: boolean = false): Promise<I_ActivityStepResult> {
    /**
     * @return is next action state
     */
    if (isPromise) {
        code = `return new Promise((resolveAction,rejectAction)=>{
            ${code}
            })`
    }

    const foo = new Function("actionState", "getAppState", "setAppState", "Utils", code)
    let state: any | I_ActivityStepResult,
        resultType: T_ActivityStepResultState = "ok"
    try {
        state = foo(actionState, getAppState, setState, Utils)
        //if return is promise  - unwrap promise
        state = await Promise.resolve(state)
        //if state is I_ActivityStepResult
        if (state && state.resultType) {
            resultType = state.resultType
            state = state.state
        }
    } catch (e) {
        state = e
        resultType = "error"
    }
    return {state: state, resultType: resultType}
}