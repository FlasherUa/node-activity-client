/// <reference path="./ActivityStepFactory_types.d.ts" />
//@ts-ignore
import runCode from './utilites/runCode';
import makeDecision from './utilites/makeDecision';
import activityLog from './utilites/activityLog';
export default class Activity {
    //(cfg: Function, runStepServer: Function, runStepClient: Function): T_ActivityClient {
    constructor(cfg) {
        this.cfg = cfg;
        this.run = this.run.bind(this);
    }
    runStepServer(name, stepCfg, state, options) { }
    runStepClient(name, stepCfg, state, options) { }
    /**
     * Main step function
     *
     * */
    ActivityStep(name, step = 0, state = {}, options = null) {
        const ActivityCfg = this.cfg()[name];
        if (!ActivityCfg) {
            throw 'Bad activity name ' + name;
        }
        const stepCfg = this.getStepCfg(ActivityCfg, step);
        if (!stepCfg) {
            return { resultType: 'completed', state };
        }
        return this.runStep(name, stepCfg, state, options);
    }
    /**
     * Run action step
     * @param name
     * @param stepCfg
     * @param state
     * @param options
     */
    runStep(name, stepCfg, state = {}, options = null) {
        if ((stepCfg.type === 'ServerApp' && this.isServer) || (stepCfg.type === 'ClientApp' && !this.isServer)) {
            //@ts-ignore
            return this.runStepLocal(name, stepCfg, state, options);
        }
        else if ((stepCfg.type === 'ClientApp' && this.isServer) ||
            (stepCfg.type === 'ServerApp' && !this.isServer)) {
            //@ts-ignore
            return this.runStepRemote(name, stepCfg, state, options);
        }
        return { resultType: 'ok', state };
    }
    /**
     * Iter-step controller
     * @param name
     * @param step
     * @param state
     * @param options
     * @constructor
     */
    async run(name, state = {}, step = 0, options = null) {
        let done = false;
        let count = step;
        /**
         * Async iterator... I don`t know how it works too...
         */
        const whileGenerator = function* () {
            while (!done) {
                yield count;
            }
        };
        const main = new Promise(async (resolve) => {
            let stepResult;
            // eslint-disable-next-line @typescript-eslint/no-unused-vars
            for (let i of whileGenerator()) {
                stepResult = await this.run_inner(name, count, state, options);
                activityLog(stepResult, 'step_result');
                count = Number(stepResult.step);
                state = stepResult.state;
                if (stepResult.resultType !== 'ok') {
                    done = true;
                }
                count++;
            }
            resolve(stepResult);
        });
        return main;
    }
    /**
     * Iter-step controller
     * @param name
     * @param step
     * @param state
     * @param options
     * @constructor
     */
    async run_inner(name, step = 0, state = {}, options = null) {
        activityLog(name + ' step: ' + step, 'step_start');
        const result = this.ActivityStep(name, step, state, options);
        // if (step===4) debugger
        let resolved = await Promise.resolve(result);
        //Action resultType logic
        if (resolved && resolved.state) {
        }
        else {
            resolved = {
                state: resolved,
                resultType: 'ok',
            };
        }
        const resultType = (resolved.resultType =
            resolved && typeof resolved.resultType !== 'undefined' ? resolved.resultType : 'ok');
        resolved.step = typeof resolved.step !== 'undefined' ? resolved.step : step;
        // resolved.name = name
        if (resultType === 'run') {
            //run a thread
            try {
                const threadResultPromise = this.run(resolved.state.activity, resolved.state, 0);
                const threadResult = await threadResultPromise;
                //return thread state & go on
                resolved.state = threadResult.state;
                resolved.resultType = 'ok';
            }
            catch (e) {
                activityLog(e, 'error');
            }
        }
        //   "ok": "error":   "completed":      "wait":  "changeActor":
        return resolved;
    }
    async run_outer() {
        const a = await Promise.resolve('1');
    }
    /**
     * Find step configuration by step number
     * @param cfg
     * @param step
     */
    getStepCfg(cfg, step) {
        return cfg[step];
    }
    async runStepLocal(activity_name, stepCfg, state) {
        let newActionState;
        /** Run action type state
         * Set Application state */
        switch (stepCfg.action) {
            case 'route':
                //try {
                activityLog(stepCfg.route, 'route');
                const route_result = this.runRoute(state, this.getAppState, this.setAppState, stepCfg.route, stepCfg.route_data);
                let promised;
                try {
                    promised = await route_result;
                }
                catch (e) {
                    console.log('EROR');
                    console.log(e);
                    debugger;
                }
                /*
                    debugger
                    if (!promised) {
                        promised = {result: promised};
                    }
                    newActionState = {resultType: 'ok', state: promised};
                } catch (e) {
                    newActionState = {resultType: 'error', state: e};
                }*/
                newActionState = promised; //{resultType: 'ok', state: route_result};
                break;
            /** Run action type code */
            case 'code':
                newActionState = runCode(state, this.getAppState, this.setAppState, stepCfg.code, stepCfg.codeIsPromise);
                break;
            case 'decision':
                newActionState = makeDecision(state, this.getAppState, this.setAppState, stepCfg.expression, stepCfg.cases);
                break;
            /* moved
           case 'decisionCase':
                debugger;
                //if case is true - go to activity named by desicion case .id
                const code1 = decisionCase(stepCfg.name, stepCfg.id);
                newActionState = runCode(state, this.getAppState, this.setAppState, code1);
                break;*/
            case 'activityLink':
                //simple include activity
                newActionState = {
                    resultType: 'run',
                    state: Object.assign(state, { activity: stepCfg.name }),
                };
                break;
        }
        /*   if (stepCfg.save_state) {
                   let out = await Promise.resolve(newActionState)
                   if (out) {
                       if (typeof out.state !== "undefined") {

                       }
                   } else {
                       out = {state: {current_result: out}}
                   }

                   out.state.prev_states || (out.state.prev_states = {})
                   out.state.prev_states[stepCfg.save_state] = state
                   return out
               }*/
        return newActionState;
    }
}
