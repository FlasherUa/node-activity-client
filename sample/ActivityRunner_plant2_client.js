import { ActivityClient } from "./node-activity-runner/client/ActivityClient";
import cfg from "./cfg/client";
//import {appStore, getAppState} from "../../../../plant-client/src/Store/AppStore";
import { fetcher } from "../../../../plant-client/src/config";
const ac_fetcher = function (stepData) {
    //@ts-ignore
    return fetcher(stepData);
};
let activityRunner;
const runRouteClient = (state, getAppState, setAppState, route, route_data) => {
    //@ts-ignore
    return {};
};
export function activityRunnerFactory(getAppState, setAppState) {
    activityRunner = new ActivityClient(cfg, getAppState, setAppState, ac_fetcher, runRouteClient);
    return activityRunner;
}
export default activityRunner;
//# sourceMappingURL=ActivityRunner_plant2_client.js.map