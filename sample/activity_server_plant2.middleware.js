import cfg from "./cfg/server";
import { ActivityServer } from "./node-activity-runner/server/ActivityServer";
import _all_controllers from "../../actions/_all_controllers";
export default function (req, res, next) {
    const query = req.query, user = req.user, body = req.body, as = new ActivityServer(cfg, null, null, null, runRouteFactory(user));
    const promise = as.run(body.activity, body.step, body.state);
    promise.then((result) => {
        res.setHeader('Content-Type', 'application/json');
        res.send(result.state);
    });
}
function runRouteFactory(user) {
    return function (state, getAppState, setAppState, route, route_data) {
        state || (state = {});
        state.__request = { user };
        const actionName = route;
        if (_all_controllers[actionName])
            return _all_controllers[actionName](null, state);
        return {};
    };
}
//# sourceMappingURL=activity_server_plant2.middleware.js.map