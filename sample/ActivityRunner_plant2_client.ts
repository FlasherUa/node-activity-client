import {ActivityClient} from "./node-activity-runner/client/ActivityClient"
import cfg from "./cfg/client"
//import {appStore, getAppState} from "../../../../plant-client/src/Store/AppStore";
import {fetcher} from "../../../../plant-client/src/config"

const ac_fetcher: F_AC_fetcher = function (stepData: I_ActivityStepMessage): Promise<I_ActivityStepMessage> {
    //@ts-ignore
    return fetcher(stepData)
}

let activityRunner: ActivityClient
const runRouteClient: F_RunRoute = (state: T_ActivityStepState, getAppState: F_GetAppState, setAppState: F_SetAppState,
                                    route: string, route_data: string): Promise<I_ActivityStepResult> => {
    //@ts-ignore
    return {}
}

export function activityRunnerFactory(getAppState, setAppState) {
    activityRunner = new ActivityClient(cfg, getAppState, setAppState, ac_fetcher, runRouteClient)
    return activityRunner
}


export default activityRunner