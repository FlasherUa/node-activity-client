//import {I_AppStore as I_ClientState} from "../../AppStore";
type I_ClientState = any

declare type I_ClientCfg = {
    [key: string]: Partial<TActivityStep>[]
}

declare interface activityComponent {
    //triggers activityNext event
    //triggers activityStop  event
}

declare type F_SetAppState = (state: any, isPartial?: boolean) => Promise<any> | void
declare type F_GetAppState = () => any
declare type F_RunRoute = (state: T_ActivityStepState, getAppState: F_GetAppState, setAppState: F_SetAppState,
                           route: string, route_data: string, save_state?: boolean) => Promise<I_ActivityStepResult>
declare type F_AC_fetcher = (step: I_ActivityStepMessage) => Promise<I_ActivityStepMessage>
declare type TActivityComponentEvent = "activityNext" | "activityStop"

/** start step object */
declare type TActivityStep = {
    id: string,//uniq name of activity step (uniq in this activity)
    no: number, //number in sequence
    label: string,//used for goto
    where?: "client" | "server",
    action: "route" | "state" | "code" | "decision" | "decisionCase" | "activityLink" | "actionLink",//or smth,
    type: "ClientApp" | "ServerApp" | string
    state?: I_ClientState | string, //state
    code?: string,
    expression?: string
    codeIsPromise?: boolean
    name?: string
    route_data?: string
    route?: string
    cases: I_Activity_Cases
    // next(): TActivityNextAction
}

interface I_Activity_Cases {
    [key: string]: I_Activity_Case
}

interface I_Activity_Case {
    id: string,
    name: string,
}

/** return from step object */
declare type TActivityNextAction = {
    type: "next" | "stop" | "goto" | "run_activity" | "parallel_goto" | "if_goto" /*showld run next & number */,
    nextId?: string | number//used for goto-like actions
    if_value?: any
    state?: T_ActivityStepState
}

declare  type T_ActivityStepState = {
    [key: string]: any
    _global?: any //global state payload for all steps in this Activity - should be passed to next state
}

declare type TActivityConfig = {
    id: string,//uniq name of activity
    steps: Array<TActivityStep>,
    permissions: Array<any>
}

declare interface IActivityRunner {
    //constructor(cfg:TActivityConfig)
    /**
     * do activity step logic
     * @param id
     */
    step(id?: string): void

    /**
     * runs activity step on client or server
     * @param stepCfg
     */
    execute(stepCfg: TActivityStep): Promise<T_ActivityStepState>
}

declare type I_ActivityStepMessage = {
    /* name of activity*/
    activity: string
    /* id (string) or of activity step */
    step: number | string
    state: T_ActivityStepState,
    resultType:T_ActivityStepResultState
}

declare type IActionRoute = {
    route: string,
    route_data: any
}


declare type   T_ActivityStepResultState = "ok" | "error" | "changeActor" | "completed" | "wait" | "run"

/**
 * return message og the step
 */
declare interface I_ActivityStepResult extends Partial<I_ActivityStepMessage> {
    resultType: T_ActivityStepResultState
}


interface I_ActivityStepFactory {
    runStepServer: Function
    runStepClient: Function
}